#include <ArduinoJson.h>
#include "Detection.h"

int LDR_value;
int BUTTON_value;
int LED_value;
int IntruptPin = 2;
void (*resetFunc)(void) = 0;

void setup() {
  Serial.begin(9600);
  pinMode(IntruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(IntruptPin), ISR_button, LOW);
  detectModule();
}

void loop() {

  LDR_value = analogRead(A0);
  BUTTON_value = analogRead(A1);
  if ((LDR_value = analogRead(A0)) > 20)
  {
    LED_value = map(LDR_value, 0, 1023, 0, 255);
    analogWrite(9, LED_value);
  }
  //delay(500);

  //Serial.println(LDR_value = analogRead(A0));
  sendLiveData(LDR_value, BUTTON_value, 0);

}
void ISR_button()
{
  resetFunc();
}
