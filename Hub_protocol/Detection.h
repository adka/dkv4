#ifndef Detection_H
#define Detection_H

String module_data;
String jsonString = ",";
bool first = true;
void detectModule()
{
  if (Serial.available() > 0)
  {
    first = true;
    module_data = Serial.readStringUntil('\n');
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(module_data);
    int count = root["C"];
    for (int i = 0; i < count; i++)
    {
      JsonObject& modules_0 = root["modules"][i];
      int TYPE = modules_0["T"]; // 102
      const char* ID = modules_0["I"]; // "101"
      const char* PORT = modules_0["P"];
      DynamicJsonBuffer toAppend;
      JsonObject& MasterSend = toAppend.createObject();
      MasterSend["TYPE"] = TYPE;
      MasterSend["ID"] = ID;
      MasterSend["PORT"] = PORT;
      String addStr;
      MasterSend.printTo(addStr);
      if (first)
        jsonString = addStr;
      else
        jsonString = jsonString + ',' + addStr;
      first = false;
      Serial.print(TYPE);
      Serial.print('\t');
      Serial.print(ID);
      Serial.print('\t');
      Serial.println(PORT);
      int pin;
      if (TYPE < 200 && TYPE > 100) // Only input module bw 100 and 199
      {
        if (PORT == 1)
        {
          pin = A0;
        }
        else if (PORT == 2)
        {
          pin = A1;
        }
        else if (PORT == 3)
        {
          pin = A2;
        }
        pinMode(pin, INPUT);
      }
      else if (TYPE < 300 && TYPE > 200)
      {
        if (PORT == 4)
        {
          pin = 9;
        }
        else if (PORT == 5)
        {
          pin = 10;
        }
        else if (PORT == 6)
        {
          pin = 11;
        }
        pinMode(pin, OUTPUT);
      }
    }
  }

}

void sendLiveData(int a, int b, int c )
{
  //
  Serial.print("{\"Sensors\":[\"\"");
  Serial.print(jsonString);
  Serial.print("],");
  //Serial.print("{\"values\);
  //"P1":155,"P2":159,"P3":0}
  //Serial.print("\"P1\":" + a + ",\"P2\":" + b + ",\"P2\":" + c + "}");
  //  Serial.print("\"P1\":" + a);
  //  Serial.print(",\"P2\":" + b);
  //  Serial.print(",\"P2\":" + c);
  //  Serial.println("}");
  DynamicJsonBuffer valueJson;
  JsonObject& Live_root = valueJson.createObject();
  Live_root["P1"] = a;
  Live_root["P2"] = b;
  Live_root["P3"] = c;
  String str;
  Live_root.printTo(str);
  str.remove(0, 1);
  Serial.println(str);
}
#endif
