#ifndef RGB_Led_H
#define RGB_Led_H

#include <Arduino.h>
#include <ArduinoJson.h>
#include <SoftwareSerial.h>

class RGB_Led

{
  public:
  
  int TYPE;
  String ID;
  int port;
  RGB_Led();
  void sendData(int,int,int);
};

#endif
