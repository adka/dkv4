#include "RGB_Led.h"

RGB_Led::RGB_Led()
{
  //TYPE = 100;
}

void RGB_Led::sendData(int r, int g, int b)
{
  SoftwareSerial mySerial(13, 11); // RX, TX
  mySerial.begin(9600);
  //Serial.begin(115200);
  String str;
  DynamicJsonBuffer Json;
  JsonObject& rgb_root = Json.createObject();
  rgb_root["R"] = r;
  rgb_root["G"] = g;
  rgb_root["B"] = b;
  rgb_root.printTo(Serial);
  rgb_root.printTo(mySerial);
  mySerial.println();
  Serial.println();

}


