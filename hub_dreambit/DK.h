#ifndef DK_H
#define DK_H

#include <ArduinoJson.h>
#include "Detection.h"
#include "Pot.h"
#include "Led.h"
#include "test.h"
#include "RGB_Led.h"



void (*resetFunc)(void) = 0;
const byte interruptPin = 3;
volatile byte state = LOW;


void ISR_button()
{
  resetFunc();
}

void Init()
{
  Serial.begin(115200);   // Print to Software
   pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), ISR_button, CHANGE);
}

#endif
