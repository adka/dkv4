#ifndef POT_H
#define POT_H

#include <Arduino.h>
#include <ArduinoJson.h>


class POT

{
  public:
  
  int TYPE;
  String ID;
  int port;
  POT();
  int VolatageLevel();
};

#endif
