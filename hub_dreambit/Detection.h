#ifndef Detection_H
#define Detection_H

#include <SoftwareSerial.h>
SoftwareSerial mySerial(2, 3);  //Recieve From Detection IC


String module_data;
String jsonString = "";
bool first = true;
bool detectModule()
{
  mySerial.begin(9600);
  if (mySerial.available() > 0)                      // To be changed to myserial
  {
    delay(50);
    first = true;
    module_data = mySerial.readStringUntil('\n');     // To be changed to myserial
    Serial.println(module_data);
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(module_data);
    int count = root["C"];
    for (int i = 0; i < count; i++)
    {
      JsonObject& modules_0 = root["modules"][i];
      int TYPE = modules_0["T"]; // 102
      const char* ID = modules_0["I"]; // "101"
      const char* PORT = modules_0["P"];
      DynamicJsonBuffer toAppend;
      JsonObject& MasterSend = toAppend.createObject();
      MasterSend["TYPE"] = TYPE;
      MasterSend["ID"] = ID;
      MasterSend["PORT"] = PORT;
      String addStr;
      MasterSend.printTo(addStr);
      if (first)
        jsonString = addStr;
      else
        jsonString = jsonString + ',' + addStr;
      first = false;
      int pin;
    }
    return false;
  }
//  mySerial.flush();
//  mySerial.end();
return true;
}

void sendLiveData(int a, int b, int c )
{
  Serial.print("{\"Sensors\":[");
  Serial.print(jsonString);
  Serial.print("],");

  DynamicJsonBuffer valueJson;
  JsonObject& Live_root = valueJson.createObject();
  Live_root["P1"] = a;
  Live_root["P2"] = b;
  Live_root["P3"] = c;
  String str;
  Live_root.printTo(str);
  str.remove(0, 1);
  Serial.println(str);
}
#endif
