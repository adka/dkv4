#ifndef LED_H
#define LED_H

#include <Arduino.h>
#include <ArduinoJson.h>


class LED

{
  public:

    int TYPE;
    String ID;
    int port;
    LED();
    void On(int);
    void Off();
};

#endif
