#include <SoftwareSerial.h>
#include <ArduinoJson.h>

SoftwareSerial mySerial(2, 11); // RX, TX
bool seperate_flag = false;
int port;
String typeList;
String previousList;
bool send_flag = false;
int count = 0;
bool dataSent = false;
void setup() {
  Serial.begin(9600);
  mySerial.begin(9600);
}

void loop() {
  DynamicJsonBuffer jsonBuffer;
  JsonObject& jsonPacket = jsonBuffer.createObject();
  typeList = "";
  if (send_flag)
    Serial.print("{\"modules\":[");
  count = 0;
  for (int i = 5; i < 10 ; i++)
  {
    pinMode(i, OUTPUT);
    digitalWrite(i, HIGH);
    delay(20);
    if (mySerial.available() > 0)
    {
      String jsonResponse = mySerial.readStringUntil('\n');
      JsonObject &package = jsonBuffer.parseObject(jsonResponse); //PASRE THE RECEIVED
      //Serial.print(jsonResponse);
            if (package.success())
            {
              count = count + 1;
              int TYPE = package["T"];
              String ID = package["I"];
              String port = String(i - 3);
              typeList = typeList + TYPE;
      
              JsonObject& root = jsonBuffer.createObject();
              root["T"] = TYPE;
              root["I"] = ID;
              root["P"] = port;
              if (send_flag)
              {
                if (seperate_flag)
                {
                  Serial.print(",");
                }
                root.printTo(Serial);
                dataSent = true;
              }
              seperate_flag = true;
            }
    }
    digitalWrite(i, LOW);
    delay(50);
  }
  //Serial.println();
    if (dataSent)
    {
      Serial.print("],");
      Serial.print("\"C\":" + String(count) + "}");
      dataSent = false;
    }
  
    send_flag = false;
    if (typeList != previousList)
    {
      Serial.println();
      send_flag = true;
      seperate_flag = false;
    }
    previousList = typeList;
  delay(100);
}
