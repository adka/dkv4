#include <SoftwareSerial.h>
#include <ArduinoJson.h>

SoftwareSerial mySerial(2, 12); // RX, TX

int port;                                         // VARIABLE TO SEE WHICH PORT MODULE IS CONNECTED
String previousList;                              // LIST OF MODULES PRESENT IN THE PREVIOUS ITERATION
bool INITIAL = true;                              // FLAG TO SEND INITIAL VALUES FOR JSON FORMAT
int count = 0;                                    // COUNT OF NUMBER OF MODULES PRESENT
String str;                                       // VARIABLE TO STORE THE PRESENT MODULES LIST
String str_vr;                                    // VIRTUAL VARIABLE TO STORE THE PRESENT MODULE LSIT
bool INITIAL_FLAG = true;                         // FLAG TO NOT ADD ',' TO THE FIRST ITERATION
int RESET_PIN = 4;
void setup() {
  Serial.begin(9600);
  mySerial.begin(9600);
  for (int i = 5; i < 10; i++)                    // DECLARING ALL LOCK PINS PINMODE AS OUTPUT
  {
    pinMode(i, OUTPUT);
    digitalWrite(i, LOW);
  }
  pinMode(RESET_PIN, OUTPUT);
}

void loop() {
  DynamicJsonBuffer jsonBuffer;
  JsonObject& jsonPacket = jsonBuffer.createObject();
  if (INITIAL)
  {
    sendInitialData();
    INITIAL = false;
  }
  count = 0;
  INITIAL_FLAG = true;
  pingForData();
  if (previousList != str)
  {
    digitalWrite(RESET_PIN, LOW);
    delay(10);
    digitalWrite(RESET_PIN, HIGH);
    delay(20);
    for (int j = 0; j < 3; j++)
    {
      sendDataSerially();
      str = "";
      str_vr = "";
      count = 0;
      sendInitialData();
      INITIAL_FLAG = true;
      pingForData();
    }
  }
  previousList = str;
  delay(100);
  INITIAL = true;
  str = "";
  str_vr = "";
}

void sendDataSerially()
{
  str = str + "]," + "\"C\":" + String(count) + "}" ;
  Serial.println(str);
}
void sendInitialData()
{
  str = "{\"modules\":[";
}
void pingForData()
{
  for (int i = 5; i < 10 ; i++)
  {
    //pinMode(i, OUTPUT);
    digitalWrite(i, HIGH);
    delay(10);
    if (mySerial.available() > 0)
    {
      String jsonResponse = mySerial.readStringUntil('\n');
      DynamicJsonBuffer jsonBuffer;
      JsonObject &package = jsonBuffer.parseObject(jsonResponse); //PASRE THE RECEIVED
      //Serial.print(jsonResponse);
      if (package.success())
      {
        count = count + 1;
        int TYPE = package["T"];
        String ID = package["I"];
        String port = String(i - 4);
        //typeList = typeList + TYPE;

        JsonObject& root = jsonBuffer.createObject();
        root["T"] = TYPE;
        root["I"] = ID;
        root["P"] = port;

        root.printTo(str_vr);
        if (INITIAL_FLAG)
        {
          str = str + str_vr;
          INITIAL_FLAG = false;
        }
        else
          str = str + ',' + str_vr;
        str_vr = "";
      }
    }
    digitalWrite(i, LOW);
    delay(3);
  }
}
