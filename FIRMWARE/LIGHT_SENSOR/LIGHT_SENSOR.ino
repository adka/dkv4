#include <ArduinoJson.h>

#define TYPE 101 // LDR
#define ID "000"
#define LOCK_PIN 6
#define OnBoardLED 7
#define LdrPin   8
#define ToggleSwitchState analogRead(0)
#define SignalOut 2
#define SerialMode 3

void setup()
{
  pinMode(LOCK_PIN, INPUT);
  pinMode(LdrPin, INPUT); // Check PINOUT is needed or not.
  pinMode(OnBoardLED, OUTPUT);
  pinMode(OnBoardLED, HIGH);
}
void loop() {
  DynamicJsonBuffer jsonBuffer;
  JsonObject& jsonPacket = jsonBuffer.createObject();
  if (digitalRead(LOCK_PIN))
  {
    digitalWrite(SerialMode, HIGH);
    Serial.begin(9600);
    jsonPacket["T"] = TYPE;
    jsonPacket["I"] = ID;
    jsonPacket.printTo(Serial);
    Serial.print('\n');
    //Serial.flush();
    Serial.end();
    delay(3);
  }

  int LightIntensity = analogRead(LdrPin); // To be given to sensor
  if (ToggleSwitchState > 0)
  {
    LightIntensity = map(LightIntensity, 0, 1023, 0, 255);
  }
  else
  {
    LightIntensity = map(LightIntensity, 0, 1023, 255, 0);
  }
  analogWrite(SignalOut, LightIntensity);    // to be given to the hub side
}

