#include <ArduinoJson.h>

#define TYPE 100 // POT
#define ID "000"
#define LOCK_PIN 6
#define OnBoardLED 7
#define SerialMode 3  //To store in  buffer


void setup()
{
  pinMode(LOCK_PIN, INPUT);
  pinMode(OnBoardLED, OUTPUT);
  pinMode(OnBoardLED, HIGH);
}
void loop() {
  DynamicJsonBuffer jsonBuffer;
  JsonObject& jsonPacket = jsonBuffer.createObject();
  if (digitalRead(LOCK_PIN))
  {
    digitalWrite(SerialMode, HIGH);
    Serial.begin(9600);
    jsonPacket["T"] = TYPE;
    jsonPacket["I"] = ID;
    jsonPacket.printTo(Serial);
    Serial.print('\n');
    //Serial.flush();
    Serial.end();
    delay(3);
  }
}
