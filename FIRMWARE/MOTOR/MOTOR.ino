#include <ArduinoJson.h>
#include <SoftwareSerial.h>


#define TYPE 203 // MOTOR
#define ID "011"
#define lockPin 5
//#define pwm_pin 3
int MOTOR_PIN1 = 10;
int MOTOR_PIN2 = 1;
int SIG_IN = 3;
int TOGGLE_PIN = 9;
bool SERIAL_MODE = false;
SoftwareSerial mySerial(SIG_IN, 12); // RX, TX

void setup() {
  Serial.begin(9600);
  mySerial.begin(9600);
  pinMode(7, OUTPUT);
  pinMode(8, INPUT);
  pinMode(6, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);

}

void loop() {
  DynamicJsonBuffer jsonBuffer;
  JsonObject& jsonPacket = jsonBuffer.createObject();
  digitalWrite(7, LOW);
  if (digitalRead(lockPin))
  {
    SERIAL_MODE = true;
    digitalWrite(7, HIGH);
    Serial.begin(9600);
    jsonPacket["T"] = TYPE;
    jsonPacket["I"] = ID;
    jsonPacket.printTo(Serial);
    Serial.print('\n');
    //Serial.flush();
    Serial.end();
    delay(3);
  }
  if (SERIAL_MODE)
  {
    if (mySerial.available() > 0)
    {
      String json = mySerial.readStringUntil('\n');
      Serial.println(json);
      DynamicJsonBuffer jsonBuffer;
      JsonObject& root = jsonBuffer.parseObject(json);
      if (!root.success()) {
        Serial.println("parseObject() failed");
        return;
      }

      int speed1 = root["speed1"];
      int speed2 = root["speed2"];
      analogWrite(MOTOR_PIN1, speed1);
      analogWrite(MOTOR_PIN2, speed2);

      // NOT SURE OF CONDITIONS
    }
    else
      workAnalogMode();
  }
}
void workAnalogMode()       // NOT SURE WHEN TO CALL
{
  int data = analogRead(3); // To be read from the hub
  if (digitalRead(TOGGLE_PIN))
  {
    analogWrite(MOTOR_PIN1, data);
    analogWrite(MOTOR_PIN2, 0);
  }
  else
  {
    analogWrite(MOTOR_PIN1, 0);
    analogWrite(MOTOR_PIN2, data);
  }
}

