#include <ArduinoJson.h>
#include <NewPing.h>

#define TYPE 102
#define ID "011"
#define lockPin 6
#define Inbuild_led 7
#define OutPutPin 2

#define TRIGGER_PIN  9
#define ECHO_PIN     10
#define MAX_DISTANCE 200 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.

void setup() {
  Serial.begin(9600);
  pinMode(Inbuild_led, OUTPUT);
  pinMode(lockPin, INPUT);
  pinMode(OutPutPin, OUTPUT);
}

void loop() {
  DynamicJsonBuffer jsonBuffer;
  JsonObject& jsonPacket = jsonBuffer.createObject();
  digitalWrite(Inbuild_led, LOW);
  if (digitalRead(lockPin))
  {
    digitalWrite(Inbuild_led, HIGH);
    Serial.begin(9600);
    jsonPacket["T"] = TYPE;
    jsonPacket["I"] = ID;
    jsonPacket.printTo(Serial);
    Serial.print('\n');
    Serial.end();
    delay(3);
  }
  int Distance = sonar.ping_cm();
  MappedDistance = map(data, 1, MAX_DISTANCE, 0, 255);
  analogWrite(OutPutPin, MappedDistance);     // to be given to the hub side
}
