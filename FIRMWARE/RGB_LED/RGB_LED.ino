#include <ArduinoJson.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(3, 12); // RX, TX
#define TYPE 202 // RGB
#define ID "011"
#define lockPin 5
//#define pwm_pin 3
int LED_R = 6;
int LED_G = 9;
int LED_B = 10;

void setup() {
  Serial.begin(9600);
  mySerial.begin(9600);
  pinMode(7, OUTPUT);
  pinMode(8, INPUT);
  pinMode(6, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);

}

void loop() {
  DynamicJsonBuffer jsonBuffer;
  JsonObject& jsonPacket = jsonBuffer.createObject();
  digitalWrite(7, LOW);
  if (digitalRead(lockPin))
  {
    digitalWrite(7, HIGH);
    Serial.begin(9600);
    jsonPacket["T"] = TYPE;
    jsonPacket["I"] = ID;
    jsonPacket.printTo(Serial);
    Serial.print('\n');
    //Serial.flush();
    Serial.end();
    delay(3);
  }
  if (mySerial.available() > 0)
  {
    String json = mySerial.readStringUntil('\n');
    Serial.println(json);
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      Serial.println("parseObject() failed");
      return;
    }

    int red = root["red"];
    int green = root["green"];
    int blue = root["blue"];

    analogWrite(LED_R, red);
    analogWrite(LED_G, green);
    analogWrite(LED_B, blue);

  }
  //  int data = analogRead(3); // To be read from the hub
  //  analogWrite(10, data);     // To be gievn to the actuators
}
