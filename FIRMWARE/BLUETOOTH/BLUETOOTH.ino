#include <ArduinoJson.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(3, 12); // RX, TX
#define TRANSMITER_TYPE 301
#define RECIEVER_TYPE 302
#define ID "001"
#define recieve_pin 5
#define send_pin 6
#define SENSOR_PIN 8
#define SIG_OUT 2
//#define pwm_pin 3
int recieve_flag = 0;
int send_flag = 0;

void Send_Mode();
void Recieve_Mode();

void setup() {
  Serial.begin(9600);
  mySerial.begin(9600);
  pinMode(7, OUTPUT);
}

void loop() {
  DynamicJsonBuffer jsonBuffer;
  JsonObject& jsonPacket = jsonBuffer.createObject();
  digitalWrite(7, LOW);
  if (digitalRead(send_pin))
  {
    //Serial.println("send_pin");
    recieve_flag = 0;
    delay(10);
    jsonPacket["T"] = TRANSMITER_TYPE;
    jsonPacket["I"] = ID;
    jsonPacket.printTo(Serial);
    Serial.print('\n');
    //Serial.flush();
    //Serial.end();
    delay(100);
    send_flag = 1;

  }
  else if (digitalRead(recieve_pin))
  {
    //Serial.println("recieve_pin");
    send_flag = 0;
    delay(10);
    jsonPacket["T"] = RECIEVER_TYPE;
    jsonPacket["I"] = ID;
    jsonPacket.printTo(Serial);
    Serial.print('\n');
    //Serial.flush();
    //Serial.end();
    delay(100);
    recieve_flag = 1;
  }
  if (send_flag)
  {
    //Serial.println("send_data");
    Serial.begin(9600);
    String json = Serial.readStringUntil('\n');
    Serial.println(json);
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      Serial.println("parseObject() failed");
      return;
    }
    String BT_Send = root["bt_Send"];
    mySerial.println(BT_Send);
    //Serial.end();
  }
  else if (recieve_flag)
  {
    //Serial.begin(9600);
    //Serial.println("recive_data");
    String BT_Recieve = mySerial.readStringUntil('\n');
    jsonPacket["bt_recieve"] = BT_Recieve;
    jsonPacket.printTo(Serial);
    Serial.println();
  }

  else
  {
    while ( mySerial.read() == 0);
    char btdata = mySerial.read();
    if (btdata == 'S') Send_Mode();
    if (btdata == 'T')Recieve_Mode();
    //else return ();
  }


}

void SendMode()
{
  int data = analogRead(3);
  mySerial.println(data);
}
void Send_Mode()
{
  int SESNOR_DATA = analogRead(SENSOR_PIN); // To be given to sensor
  SESNOR_DATA = map(SESNOR_DATA, 0, 255, 0, 100);
  mySerial.println(SESNOR_DATA);
}
void Recieve_Mode()
{
  int BT_Recieve = mySerial.parseInt();
  analogWrite(SIG_OUT, BT_Recieve);
}
