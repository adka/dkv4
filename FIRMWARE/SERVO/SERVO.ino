#include <ArduinoJson.h>
#include <Servo.h>

Servo myservo;
#define TYPE 206 // SERVO
#define ID "011"
#define LOCK_PIN 6
#define SIG_IN 3
#define ACTUATOR_PIN 9 //signal out direct from sig in(SO DAC for sensor)

void setup() {
  Serial.begin(9600);
  pinMode(LOCK_PIN, INPUT);
  myservo.attach(ACTUATOR_PIN);

}

void loop() {
  DynamicJsonBuffer jsonBuffer;
  JsonObject& jsonPacket = jsonBuffer.createObject();
  if (digitalRead(LOCK_PIN))
  {
    Serial.begin(9600);
    jsonPacket["T"] = TYPE;
    jsonPacket["I"] = ID;
    jsonPacket.printTo(Serial);
    Serial.print('\n');
    //Serial.flush();
    Serial.end();
    delay(3);
  }
  int IN_DATA = analogRead(SIG_IN); // To be read from the hub
  int SERVO_IN =  map(IN_DATA, 0, 1024, 0, 180);
  myservo.write(SERVO_IN);     // To be gievn to the actuators
}
