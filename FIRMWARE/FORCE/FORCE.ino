#include <ArduinoJson.h>

#define TYPE 118 // FORCE
#define ID "000"
#define LOCK_PIN 6
#define SIG_IN 3
#define SENSOR_PIN 8
#define SIG_OUT 2


void setup() {
  Serial.begin(9600);
  pinMode(LOCK_PIN, INPUT);

}
void loop() {
  DynamicJsonBuffer jsonBuffer;
  JsonObject& jsonPacket = jsonBuffer.createObject();
  if (digitalRead(LOCK_PIN))
  {
    Serial.begin(9600);
    jsonPacket["T"] = TYPE;
    jsonPacket["I"] = ID;
    jsonPacket.printTo(Serial);
    Serial.print('\n');
    //Serial.flush();
    Serial.end();
    delay(3);
  }
    int SESNOR_DATA = analogRead(SENSOR_PIN); // To be given to sensor
    SESNOR_DATA = map(SESNOR_DATA, 0, 1023, 0, 255);
    analogWrite(SIG_OUT, SESNOR_DATA);     // to be given to the hub side
}
