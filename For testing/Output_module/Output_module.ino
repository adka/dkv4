#include <ArduinoJson.h>

#define TYPE 102
#define ID "011"
#define lockPin 6
//#define pwm_pin 3


void setup() {
  Serial.begin(9600);
  pinMode(7, OUTPUT);
  pinMode(6, INPUT);
  pinMode(3, INPUT);
  pinMode(10, OUTPUT);

}

void loop() {
      DynamicJsonBuffer jsonBuffer;
      JsonObject& jsonPacket = jsonBuffer.createObject();
      digitalWrite(7, LOW);
      if (digitalRead(lockPin))
      {
        digitalWrite(7, HIGH);
        Serial.begin(9600);
        jsonPacket["T"] = TYPE;
        jsonPacket["I"] = ID;
        jsonPacket.printTo(Serial);
        Serial.print("\n");
        Serial.flush();
        Serial.end();
        delay(10);
      }
  int data = analogRead(3); // To be read from the hub
  Serial.println(data);
  analogWrite(10, data);     // To be gievn to the actuators
}
